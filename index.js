const { Client } = require("discord.js")
const { config } = require("dotenv");

const client = new Client({
    disableEveryone: true
});

config({
    path: __dirname + "/.env"
});

client.on("ready", () => {
    console.log("Harpia bot online!");
});

client.on("message", async message => {
    if (message.author.bot) return;
    if (!message.guild) return;
    if (!message.content.startsWith(process.env.PREFIX)) return;

    require("./commands/commandSay")(message, client);
    require("./commands/commandHelp")(message, client);
    require("./commands/commandYoutube")(message, client);
    require("./commands/commandTwitch")(message, client);
    require("./commands/commandInstagram")(message, client);
    require("./commands/commandTwitter")(message, client);
    require("./commands/commandTell")(message, client);
    require("./commands/commandGetRolesId")(message, client);
    require("./commands/commandGetChatId")(message, client);
    require("./commands/commandClean")(message, client);
    require("./commands/commandAnnounce")(message, client);
});

require("./events/antiFloodEvent")(client);
require("./events/messageDeletedEvent")(client);
require("./events/memberJoinEvent")(client);
require("./events/memberQuitEvent")(client);

client.login(process.env.TOKEN);