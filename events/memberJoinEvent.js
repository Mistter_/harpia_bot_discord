module.exports = async (client) => {
    client.on("guildMemberAdd", (member) => {
        client.channels.fetch(process.env.CHANNEL_LOGS_LOGIN)
        .then(channel => channel.send(`${member} :inbox_tray:`));
        member.send(`${member} Olá, seja bem-vindo ao Discord oficial da Harpia Studios! Aproveite para seguir nosso Twitter https://twitter.com/Harpiabrazil e Instagram https://www.instagram.com/harpiabrazil/ e fique atualizado com todas as últimas notícias de nossa equipe.`);
        member.guild.roles.fetch(process.env.ROLE_MEMBER_ID).then(role => member.roles.add(role)).catch(console.error);
    });
}