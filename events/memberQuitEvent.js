module.exports = async (client) => {
    client.on("guildMemberRemove", (member) => {
        client.channels.fetch(process.env.CHANNEL_LOGS_LOGIN)
        .then(channel => channel.send(`${member} :outbox_tray:`));
    });
}