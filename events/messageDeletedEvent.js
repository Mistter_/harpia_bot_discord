module.exports = async (client) => {
    client.on('messageDelete', message => {
        client.channels.fetch(process.env.CHANNEL_LOGS_CHAT)
        .then(channel => channel.send(`Autor da mensagem: ${message.author}\nMensagem: ${message}`));
    });

    client.on('messageDeleteBulk', message => {
        client.channels.fetch(process.env.CHANNEL_LOGS_CHAT)
        .then(channel => channel.send(`Autor da mensagem: ${message.author}\nMensagem: ${message}`));
    });
}