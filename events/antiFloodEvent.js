module.exports = async (client) => {
    client.on("message", async message => {
        const fs = require("fs");
        let antifloodDb = require("../database/antifloodDb.json");

        if (message.author.bot) return;
        if (!message.guild) return;

        if (!message.member.roles.cache.has(process.env.ROLE_MEMBER_ID))
        return;

        //time in seconds
        var antifloodTime = 5;

        if (!antifloodDb.hasOwnProperty(message.author.id)) {
            antifloodDb[message.author.id] = {
                lastMessage: new Date().getTime()
            };
            fs.writeFile("./database/antifloodDb.json", JSON.stringify(antifloodDb), (err) => {
                if (err) console.log(err);
            });
        } else {
            if (((new Date().getTime() - antifloodDb[message.author.id].lastMessage) / 1000) < antifloodTime) {
                if (message.deletable) message.delete();
            } else {
                antifloodDb[message.author.id] = {
                    lastMessage: new Date().getTime()
                }
                fs.writeFile("./database/antifloodDb.json", JSON.stringify(antifloodDb), (err) => {
                    if (err) console.log(err);
                });
            }
        }
    });
}