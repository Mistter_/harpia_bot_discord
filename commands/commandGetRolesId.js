module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (command === "getrolesid") {
        if (message.member.roles.cache.has(process.env.ROLE_MEMBER_ID)) 
        return;
        
        var msg = null;
        message.guild.roles.cache.find(role => {
            if (msg == null) {
                msg = `\nCargo: ${role.name}\nID: ${role.id}`;
            } else msg += `\n \nCargo: ${role.name}\nID: ${role.id}`;
        });

        if (message.deletable) message.delete();

        if (args.length > 1 || args.length == 0) 
        return message.reply(`Ops o comando contém algum erro.\n Utilize: !getrolesid`).then(m => m.delete({
            timeout: 10000,
            reason: ""
        }));

        message.reply(msg).then(m => m.delete({
            timeout: 20000,
            reason: ""
        }));
    }
}