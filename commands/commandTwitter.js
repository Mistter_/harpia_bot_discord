module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (command === "twitter") {
        if (message.deletable) message.delete();
        
        if (args.length > 0)
        return message.reply(`Ops o comando contém algum erro.\n Utilize: !twitter`).then(m => m.delete({
            timeout: 10000,
            reason: ""
        }));

        message.guild.members.cache.get(message.author.id).send(`Olá você solicitou nosso twitter, segue a thread com o mesmo: \nhttps://twitter.com/Harpiabrazil`);
    }
}