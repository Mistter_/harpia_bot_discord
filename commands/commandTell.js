module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // /tell (nick) (local/privado) (msg)
    if (command === "tell" || command === "falar") {
        if (message.member.roles.cache.has(process.env.ROLE_MEMBER_ID))
            return;

        if (message.deletable) message.delete();

        if (args.length < 3)
            return message.reply(`Ops o comando contém algum erro.\n Utilize: !falar <usuário> <local/privado> <mensagem>`).then(m => m.delete({
                timeout: 10000,
                reason: ""
            }));

        var msg = message.content.split(" ");
        msg.splice(0, 3);
        msg = msg.join(" ");

        if (args[1] === "local") {
            message.channel.send(`${args[0]} ${msg}`);
        } else if (args[1] === "privado" || args[1] === "private") {
            message.member.guild.members.cache.find(user => {
                if (`<@!${user.id}>` === args[0]) {
                    message.guild.members.cache.get(user.id).send(msg);
                }
            });
        } else
            return message.reply(`Ops o comando contém algum erro.\n Utilize !falar <usuário> <local/privado> <mensagem>`).then(m => m.delete({
                timeout: 10000,
                reason: ""
            }));
    }
}