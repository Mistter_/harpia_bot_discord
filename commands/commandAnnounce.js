module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // /anunciar (aqui[opcional]) (msg)
    if (command === "anunciar" || command === "announce") {
        if (message.member.roles.cache.has(process.env.ROLE_MEMBER_ID))
            return;

        if (message.deletable) message.delete();

        if (args.length < 1)
            return message.reply("Nada para anúnciar?").then(m => m.delete({
                timeout: 10000,
                reason: ""
            }))

        if (args[0] === "aqui" || args[0] === "here") {
            if (args.length < 2)
                return message.reply("Nada para anúnciar?").then(m => m.delete({
                    timeout: 10000,
                    reason: ""
                }))

            var msg = message.content.split(" ");
            msg.splice(0, 2);
            msg = msg.join(" ");
            message.channel.send(`@everyone ${msg}`);
        } else {
            var msg = message.content.split(" ");
            msg.splice(0, 1);
            msg = msg.join(" ");
            client.channels.fetch(process.env.CHANNEL_AVISOS_ID)
                .then(channel => channel.send(`@everyone ${msg}`))
                .then(channel => message.reply(`Anúncio efetuado! (${channel.channel})`));
        }
    }
}