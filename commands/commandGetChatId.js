module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (command === "getchatid") {
        if (message.member.roles.cache.has(process.env.ROLE_MEMBER_ID)) 
        return;

        if (message.deletable) message.delete();

        if (args.length > 1 || args.length == 0) 
        return message.reply(`Ops o comando contém algum erro.\n Utilize: !getchatid`).then(m => m.delete({
            timeout: 10000,
            reason: ""
        }));

        message.reply(`ID do chat: ${message.channel.id}`).then(m => m.delete({
            timeout: 20000,
            reason: ""
        }));
    }
}