module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (command === "youtube") {
        if (message.deletable) message.delete();
        
        if (args.length > 0)
        return message.reply(`Ops o comando contém algum erro.\n Utilize: !youtube`).then(m => m.delete({
            timeout: 10000,
            reason: ""
        }));

        message.guild.members.cache.get(message.author.id).send(`Olá você solicitou nosso canal do youtube, segue a thread com o mesmo: \nhttps://www.youtube.com/channel/UCVQNx8D633SxzQCmLZ1KoNw`);
    }
}