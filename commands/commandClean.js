module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    if (command === "clear" || command === "limpar") {
        if (message.member.roles.cache.has(process.env.ROLE_MEMBER_ID))
            return;

        if (args.length > 1 || args.length == 0)
            return message.reply(`Ops o comando contém algum erro.\n Utilize: !limpar <linhas>`).then(m => m.delete({
                timeout: 10000,
                reason: ""
            }));

        message.channel.bulkDelete(args[0]).then(() => {
            message.channel.send(`Foram limpas ${args[0]} mensagens.`).then(m => m.delete({
                timeout: 10000,
                reason: ""
            }));
        });
    }
}