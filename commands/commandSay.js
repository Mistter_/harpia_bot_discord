module.exports = async (message, client) => {
    const args = message.content.slice(process.env.PREFIX.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    const Discord = require('discord.js');

    // /say (normal/embed) (everyone) (mensagem)
    if (command === "say" || command === "dizer") {
        if (message.member.roles.cache.has(process.env.ROLE_MEMBER_ID))
            return;

        if (message.deletable) message.delete();

        if (args.length < 2)
            return message.reply(`Ops o comando contém algum erro.\n Utilize: !dizer <normal/embed> <mensagem>`).then(m => m.delete({
                timeout: 10000,
                reason: ""
            }));

        if (args[0] === "normal") {
            var msg = message.content.split(" ");
            msg.splice(0, 2);
            msg = msg.join(" ");

            message.channel.send(msg);
        } else if (args[0] === "embed") {
            if (args.length < 3)
                return message.reply(`Ops o comando contém algum erro.\n Utilize: !dizer <normal/embed> <everyone> <mensagem>`).then(m => m.delete({
                    timeout: 10000,
                    reason: ""
                }));

            var msg = message.content.split(" ");
            msg.splice(0, 3);
            msg = msg.join(" ");

            var everyone = false;
            if (args[1] === "true" || args[1] === "verdadeiro") everyone = true; 

            const embed = new Discord.MessageEmbed()
                .setColor("RANDOM")
                .setDescription(msg);

            if (everyone == true) {
                message.channel.send(embed);
                message.channel.send(`@everyone \n`);
            } else message.channel.send(embed);
        } else
            return message.reply(`Ops o comando contém algum erro.\n Utilize: !dizer <normal/embed> <mensagem>`).then(m => m.delete({
                timeout: 10000,
                reason: ""
            }));
    }
}